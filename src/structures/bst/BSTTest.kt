package structures.bst

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

@Suppress("FunctionName")
internal class BSTTest {

	@Nested
	inner class SimpleTests {
		lateinit var bst: BST

		@BeforeEach
		fun setUp() {
			bst = BST()
		}

		@Test
		fun `bst is empty`() {
			assert(bst.asListInOrder() == listOf<Int>())
			assert(bst.size == 0)
		}
	}

	@Nested
	inner class InsertionTests {
		lateinit var bst: BST

		@BeforeEach
		fun setUp() {
			bst = BST()
		}

		@Test
		fun `bst all one number`() {
			bst.insert(50)
			bst.insert(50)
			bst.insert(50)
			assert(bst.size == 3)
			assert(bst.asListInOrder() == listOf(50, 50, 50))
		}

		@Test
		fun `add more numbers`() {
			bst.insert(50)
			bst.insert(40)
			bst.insert(45)
			bst.insert(35)
			bst.insert(43)
			bst.insert(47)
			bst.insert(46)
			bst.insert(60)
			bst.insert(70)
			bst.insert(80)
			bst.insert(75)
			bst.insert(85)
			bst.insert(61)
			bst.insert(60)
			assert(bst.size == 14)
			assert(bst.asListInOrder() == listOf(35, 40, 43, 45, 46, 47, 50, 60, 60, 61, 70, 75, 80, 85))
		}
	}

	@Nested
	inner class ContainsTests {
		lateinit var bst: BST

		@BeforeEach
		fun setUp() {
			bst = BST()
		}

		@Test
		fun `contains on empty`() {
			assert(!bst.contains(100))
			assert(!bst.contains(1))
			assert(!bst.contains(0))
		}

		@Test
		fun `contains with larger list`() {
			bst.insert(50)
			bst.insert(40)
			bst.insert(45)
			bst.insert(35)
			bst.insert(43)
			bst.insert(47)
			bst.insert(46)
			bst.insert(60)
			bst.insert(70)
			bst.insert(80)
			bst.insert(75)
			bst.insert(85)
			bst.insert(61)
			bst.insert(60)
			assert(!bst.contains(100))
			assert(!bst.contains(1))
			assert(!bst.contains(0))
			assert(bst.contains(85))
			assert(bst.contains(50))
			assert(bst.contains(46))
		}
	}
}