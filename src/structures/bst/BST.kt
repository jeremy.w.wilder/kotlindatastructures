package structures.bst

class BST {

	private var head: Node? = null
	private var _size: Int = 0
	val size: Int
		get() = _size

	fun insert(i: Int) {
		if (head == null) {
			head = Node(i)
			_size += 1
		} else {
			insert(i, head!!)
		}
	}

	private fun insert(i: Int, node: Node) {
		when {
			i <= node.num -> {
				node.left?.let {
					insert(i, it)
				} ?: run {
					node.left = Node(i)
					_size += 1
				}
			}
			else -> {
				node.right?.let {
					insert(i, it)
				} ?: run {
					node.right = Node(i)
					_size += 1
				}
			}
		}
	}

	fun contains(i: Int): Boolean {
		return head?.let {
			if (contains(i, it)) {
				println("The BST contains $i")
				true
			} else {
				println("The BST doesn't contain $i")
				false
			}
		} ?: run {
			println("The BST doesn't contain $i")
			false
		}
	}

	private fun contains(i: Int, node: Node): Boolean {
		return when {
			i == node.num -> true
			i <= node.num -> node.left?.let {
				contains(i, it)
			}
			i >= node.num -> node.right?.let {
				contains(i, it)
			}
			else -> false
		} ?: false
	}

	/**
	 * Returns the BST as an in-order list
	 * Mostly for unit testing
	 */
	fun asListInOrder() : List<Int> {
		val mutList = mutableListOf<Int>()
		head?.let {
			asListInOrder(it,mutList)
		}
		return mutList.toList()
	}

	private fun asListInOrder(node: Node, list: MutableList<Int>) {
		node.left?.let {
			asListInOrder(it,list)
		}
		list.add(node.num)
		node.right?.let {
			asListInOrder(it,list)
		}
	}

	fun printInOrder() {
		head?.let {
			printInOrder(it)
		}
	}

	private fun printInOrder(node: Node) {
		node.left?.let {
			printInOrder(it)
		}
		println(node.num)
		node.right?.let {
			printInOrder(it)
		}
	}

	class Node(val num: Int, var left: Node? = null, var right: Node? = null)
}