import structures.bst.BST

fun main(args: Array<String>) {

	val bst = BST()

	bst.insert(50)
	bst.insert(40)
	bst.insert(45)
	bst.insert(35)
	bst.insert(43)
	bst.insert(47)
	bst.insert(46)
	bst.insert(60)
	bst.insert(70)
	bst.insert(80)
	bst.insert(75)
	bst.insert(85)
	bst.insert(61)
	bst.insert(60)

	bst.printInOrder()

	val inOrderList = bst.asListInOrder()
	println(inOrderList)
}